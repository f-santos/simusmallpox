# simusmallpox 0.0.2 (Release date: 2020-05-18)

## Minor fixes

Improved display of dataframe of results in `simu_smallpox()` output.

# simusmallpox 0.0.1 

First release

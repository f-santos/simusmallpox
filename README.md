simusmallpox
============

This package is still at a very early stage of development and is not yet for public use.

## Installation of the R package `simusmallpox` from GitLab, using `remotes`

#### Install prerequisites

1. Install the R package `remotes` by typing the following command line into the R console:
   ```r
   install.packages("remotes", dep = TRUE)
   ```

2. Install build environment
   * **Linux** : no additional operation required (but make sure that [gcc](https://gcc.gnu.org/viewcvs/gcc/) is installed on your system).
   * **OSX:** make sure that [XCODE](https://developer.apple.com/xcode/) is installed.
   * **Windows:** install the latest version of [Rtools](https://cran.r-project.org/bin/windows/Rtools/). During the installation process, make sure to select *"Edit the system path"*.

#### Installing anthrostat

Run the following command in R:
```r
remotes::install_git('https://gitlab.com/f-santos/simusmallpox.git')
```
        

